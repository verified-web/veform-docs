# veform

Veform is a [Vue](https://vuejs.org/) component & theme library for creating modern web forms based on [Bootstrap 4](https://getbootstrap.com/docs/4.3/)

### Getting started
Install veform using npm
```bash
npm install veform
```

Using veform in your Vue project
```javascript
// main.js
import Vue from 'vue'
import App from './App.vue'
import veform from 'veform'

import "./style.scss"

Vue.use(veform)

new Vue({
    render: h => h(App),
}).$mount('#app')
```

```scss
// style.scss
@import "./node_modules/veform/themes/default/style.scss";
```

You can use style.scss to override any of the available Bootstrap 4 sass variables.

### Example components
```html
<!-- Input with some validations -->
<ve-col xs12 sm6>
  <ve-form-group>
    <label>Organization number:</label>
    <ve-input v-model="ud.company_name" rules="required|min:9|max:9"></ve-input>
  </ve-form-group>
</ve-col>
```
![Image of input from code above](https://i.imgur.com/NRZ16a5.png)

```html
<!-- Stepper component -->
<ve-stepper :steps="3" :step="step" @step-click="previousStep" @btn-next-click="nextStep"></ve-stepper>
```
![Image of stepper from code above](https://i.imgur.com/wbNahgS.png)

### Custom validations
Adding custom validation rules is as easy as:
```javascript
veform.addCustomValidation('uppercase', (value, constraint) => {

    if(value.toUpperCase() === value) return { status: true }

    return { status: false, key: "validation.uppercase" }

})
```

### Input masking
Input fields can be masked by including the `mask` prop with a combination of the following placeholders:

| Value | Format                       |
|-------|------------------------------|
| #     | Number (0-9)                 |
| A     | Letter in any case (a-z, A-Z)|
| N     | Number or letter             |
| X     | Any symbol                   |
| ?     | Optional (next character)    |

```html
<ve-input v-model="orgNr" mask="### ### ###" rules="required|min:11"></ve-input>
```

### Custom locales
Veform implements a global mixin with the following 3 methods for handling locales:

```javascript
setIso('en_EN') // Page language is set to English
setIso('nb_NO') // Page language is set to Norwegian
```

```javascript
// mergeLocale let's you expand on and overwrite the default translation strings of veform.
mergeLocale({
  "validation.uppercase": {
    "en_EN": "Must be uppercase",
    "nb_NO": "Kan kun inneholde store bokstaver"
  }
})
```

```javascript
// Returns the translation string for a specified key based on current language
__('validation.uppercase')
__('email')

// Sometimes you want to add a variable to a translation string.
// All extra parameters will replace %s in the string
// let's say we have a greeting translation: "Hello, %s"
__('greeting', 'Bob') // outputs: Hello, Bob
```

_Note that when using these methods outside of template literals, `this.` should be prepended_



### Versioning
```
"version": "1.1.2" (x.y.z)

x: New major release
y: Breaking change
z: Added functionality & patches
```

### Contributing
Feel free to contribute by creating a new branch/fork and submit a pull request.

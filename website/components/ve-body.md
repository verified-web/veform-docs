# ve-body

The default veform body component

## Slots

<!-- @vuese:ve-body:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|-|-|

<!-- @vuese:ve-body:slots:end -->



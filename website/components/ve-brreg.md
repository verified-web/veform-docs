# ve-brreg

Demo: https://demo-veform.web.verified.eu/#/ve-brreg

## Props

<!-- @vuese:ve-brreg:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|-|`String`|`false`|-|
|readonly|-|`Boolean`|`false`|-|
|disabled|-|`Boolean`|`false`|-|
|rules|-|`String`|`false`|-|
|placeholder|-|`String`|`false`|-|

<!-- @vuese:ve-brreg:props:end -->


## Events

<!-- @vuese:ve-brreg:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|

<!-- @vuese:ve-brreg:events:end -->



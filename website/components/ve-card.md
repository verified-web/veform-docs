# ve-card

Demo: https://demo-veform.web.verified.eu/#/ve-card

## Props

<!-- @vuese:ve-card:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|headline|Adds a header block on top of the card object and the header is set to the content of headline variable.|`String`|`false`|-|

<!-- @vuese:ve-card:props:end -->


## Slots

<!-- @vuese:ve-card:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|-|-|

<!-- @vuese:ve-card:slots:end -->



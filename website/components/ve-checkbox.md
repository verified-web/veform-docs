# ve-checkbox

Demo: https://demo-veform.web.verified.eu/#/ve-checkbox

## Props

<!-- @vuese:ve-checkbox:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|-|`Object`|`false`|{}|
|options|-|`Object`|`false`|-|
|rules|-|`String`|`false`|-|
|inline|-|`Boolean`|`false`|-|
|disabled|-|`Boolean`|`false`|-|

<!-- @vuese:ve-checkbox:props:end -->


## Events

<!-- @vuese:ve-checkbox:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|

<!-- @vuese:ve-checkbox:events:end -->



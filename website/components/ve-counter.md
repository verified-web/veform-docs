# ve-counter

Demo: https://demo-veform.web.verified.eu/#/ve-counter

## Props

<!-- @vuese:ve-counter:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|length|Sets the current length of the list object associated with the counter.|`Number`|`false`|-|
|max|The max value of the counter.|`Number`|`false`|10|
|min|The min value of the counter.|`Number`|`false`|-|

<!-- @vuese:ve-counter:props:end -->


## Events

<!-- @vuese:ve-counter:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|on-add|Event is dispatched when the user clicks on the "+" counter button|-|
|on-remove|Event is dispatched when the user clicks on the "-" counter button|-|

<!-- @vuese:ve-counter:events:end -->



# ve-datepicker

Demo: https://demo-veform.web.verified.eu/#/ve-datepicker

## Props

<!-- @vuese:ve-datepicker:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|-|`String`|`false`|-|
|placeholder|-|`String`|`false`|Velg dato|
|rules|-|`String`|`false`|-|
|required|-|`Boolean`|`false`|-|
|language|-|`String`|`false`|en|
|format|-|`String`|`false`|numeric|
|outputISO|-|`Boolean`|`false`|-|
|debug|-|`Boolean`|`false`|-|
|openDate|-|`Date`|`false`|-|
|disabledDates|-|`Object`|`false`|-|

<!-- @vuese:ve-datepicker:props:end -->


## Events

<!-- @vuese:ve-datepicker:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|

<!-- @vuese:ve-datepicker:events:end -->



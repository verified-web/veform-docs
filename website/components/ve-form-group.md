# ve-form-group

Use to group smaller form objects.

## Slots

<!-- @vuese:ve-form-group:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|-|-|

<!-- @vuese:ve-form-group:slots:end -->



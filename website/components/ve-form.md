# ve-form

Default form component.

## Slots

<!-- @vuese:ve-form:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|-|-|

<!-- @vuese:ve-form:slots:end -->



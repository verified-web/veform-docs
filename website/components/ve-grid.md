# ve-grid

## ve-col and ve-row

The columns and rows of ve-grid. <br>
The props are used to define the size of each column

## Props

<!-- @vuese:ve-col:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|xs1|-|`Boolean`|`false`|-|
|xs2|-|`Boolean`|`false`|-|
|xs3|-|`Boolean`|`false`|-|
|xs4|-|`Boolean`|`false`|-|
|xs5|-|`Boolean`|`false`|-|
|xs6|-|`Boolean`|`false`|-|
|xs7|-|`Boolean`|`false`|-|
|xs8|-|`Boolean`|`false`|-|
|xs9|-|`Boolean`|`false`|-|
|xs10|-|`Boolean`|`false`|-|
|xs11|-|`Boolean`|`false`|-|
|xs12|-|`Boolean`|`false`|-|
|sm1|-|`Boolean`|`false`|-|
|sm2|-|`Boolean`|`false`|-|
|sm3|-|`Boolean`|`false`|-|
|sm4|-|`Boolean`|`false`|-|
|sm5|-|`Boolean`|`false`|-|
|sm6|-|`Boolean`|`false`|-|
|sm7|-|`Boolean`|`false`|-|
|sm8|-|`Boolean`|`false`|-|
|sm9|-|`Boolean`|`false`|-|
|sm10|-|`Boolean`|`false`|-|
|sm11|-|`Boolean`|`false`|-|
|sm12|-|`Boolean`|`false`|-|
|md1|-|`Boolean`|`false`|-|
|md2|-|`Boolean`|`false`|-|
|md3|-|`Boolean`|`false`|-|
|md4|-|`Boolean`|`false`|-|
|md5|-|`Boolean`|`false`|-|
|md6|-|`Boolean`|`false`|-|
|md7|-|`Boolean`|`false`|-|
|md8|-|`Boolean`|`false`|-|
|md9|-|`Boolean`|`false`|-|
|md10|-|`Boolean`|`false`|-|
|md11|-|`Boolean`|`false`|-|
|md12|-|`Boolean`|`false`|-|
|lg1|-|`Boolean`|`false`|-|
|lg2|-|`Boolean`|`false`|-|
|lg3|-|`Boolean`|`false`|-|
|lg4|-|`Boolean`|`false`|-|
|lg5|-|`Boolean`|`false`|-|
|lg6|-|`Boolean`|`false`|-|
|lg7|-|`Boolean`|`false`|-|
|lg8|-|`Boolean`|`false`|-|
|lg9|-|`Boolean`|`false`|-|
|lg10|-|`Boolean`|`false`|-|
|lg11|-|`Boolean`|`false`|-|
|lg12|-|`Boolean`|`false`|-|
|xl1|-|`Boolean`|`false`|-|
|xl2|-|`Boolean`|`false`|-|
|xl3|-|`Boolean`|`false`|-|
|xl4|-|`Boolean`|`false`|-|
|xl5|-|`Boolean`|`false`|-|
|xl6|-|`Boolean`|`false`|-|
|xl7|-|`Boolean`|`false`|-|
|xl8|-|`Boolean`|`false`|-|
|xl9|-|`Boolean`|`false`|-|
|xl10|-|`Boolean`|`false`|-|
|xl11|-|`Boolean`|`false`|-|
|xl12|-|`Boolean`|`false`|-|

<!-- @vuese:ve-col:props:end -->
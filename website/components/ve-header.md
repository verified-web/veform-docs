# ve-header

Demo: https://demo-veform.web.verified.eu/#/ve-header

## Slots

<!-- @vuese:ve-header:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|veform header|-|

<!-- @vuese:ve-header:slots:end -->



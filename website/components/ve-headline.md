# ve-headline

Demo: https://demo-veform.web.verified.eu/#/ve-headline

## Props

<!-- @vuese:ve-headline:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|headline|The headline text|`String`|`false`|-|
|size|Custom definition of font-size|`String`|`false`|-|
|tip|Defines the tooltip object|`Object`|`false`|-|

<!-- @vuese:ve-headline:props:end -->



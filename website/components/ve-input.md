# ve-input

Demo: https://demo-veform.web.verified.eu/#/ve-input

## Props

<!-- @vuese:ve-input:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|-|`String` /  `Number`|`false`|-|
|type|-|`String`|`false`|-|
|rules|-|`String`|`false`|-|
|placeholder|Define a placeholder text|`String`|`false`|-|
|mask|-|—|`false`|-|
|dictionary|-|`Object`|`false`|-|
|disabled|Disable the input field|`Boolean`|`false`|-|
|readonly|-|`Boolean`|`false`|-|
|autocomplete|-|`String`|`false`|-|
|blurvalidate|-|`Boolean`|`false`|-|

<!-- @vuese:ve-input:props:end -->


## Events

<!-- @vuese:ve-input:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|
|on-dictionary-match|-|-|
|on-blur|-|-|

<!-- @vuese:ve-input:events:end -->



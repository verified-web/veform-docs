# ve-label

Demo: https://demo-veform.web.verified.eu/#/ve-label

Documentation: https://getbootstrap.com/docs/4.1/components/tooltips/

## Props

<!-- @vuese:ve-label:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|required|-|`Boolean`|`false`|-|
|label|-|`String`|`false`|-|
|tip|-|`Object`|`false`|-|

<!-- @vuese:ve-label:props:end -->


## Slots

<!-- @vuese:ve-label:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|-|-|

<!-- @vuese:ve-label:slots:end -->



# ve-radio

Demo: https://demo-veform.web.verified.eu/#/ve-radio

## Props

<!-- @vuese:ve-radio:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|-|`Object`|`false`|{}|
|options|-|`Object`|`false`|-|
|rules|-|`String`|`false`|-|
|inline|-|`Boolean`|`false`|-|
|disabled|-|`Boolean`|`false`|-|

<!-- @vuese:ve-radio:props:end -->


## Events

<!-- @vuese:ve-radio:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|
|radio-click|-|-|

<!-- @vuese:ve-radio:events:end -->



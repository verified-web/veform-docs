# ve-screening

## Props

<!-- @vuese:ve-screening:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|orgNr|Which company should be searched on. Changing this will trigger company search|`String` /  `Number`|`false`|-|
|people|For programatically adding people without using company search|`Array`|`false`|[]|
|enableSearchBar|Controls if the company search bar is enabled|`Boolean`|`false`|-|
|enableShadow|Controls shadow on the person container cards and submit button|`Boolean`|`false`|-|

<!-- @vuese:ve-screening:props:end -->


## Events

<!-- @vuese:ve-screening:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|exception|-|-|
|aml|-|-|
|submit|-|-|

<!-- @vuese:ve-screening:events:end -->



# ve-select

Demo: https://demo-veform.web.verified.eu/#/ve-select

## Props

<!-- @vuese:ve-select:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|disabled|Disables the input field|`Boolean`|`false`|-|
|disabledProp|-|`String`|`false`|disabled|
|options|The different options of the select|`String`[] or `Object`<text, value>[]|`false`|null|
|searchable|Whether or not it should be possible to search for the different options|`Boolean`|`false`|-|
|showDefaultOption|-|`Boolean`|`false`|-|
|textProp|-|`String`|`false`|text|
|value|-|`Object` /  `String` /  `Number`|`false`|-|
|valueProp|-|`String`|`false`|value|
|rules|Validation rules|`String`|`false`|-|
|icon|A fontawesome icon, e.g. "fas fa-wrench" that will be displayed in the search field.|`String`|`false`|-|
|iconColor|Color of the icon|`String`|`false`|#333|
|loading|Whether or not to display a spinner/loader in the search field|`Boolean`|`false`|-|
|ignoreFilter|Show all options even when something is entered in the search field|`Boolean`|`false`|-|
|placeholder|Overwrite the default text shown in the input|`String`|`false`|-|
|selectedIndex|-|`Number`|`false`|-|

<!-- @vuese:ve-select:props:end -->


## Events

<!-- @vuese:ve-select:events:start -->
|Event Name|Description|Type|
|---|---|---|
|on-select|Event is dispatched when the user clicks on an option|`String`or `Object`|
|on-search|Event is dispatched when the user types in the search input|`String`|

<!-- @vuese:ve-select:events:end -->


# ve-stepper

Demo: https://demo-veform.web.verified.eu/#/ve-stepper

## Props

<!-- @vuese:ve-stepper:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|steps|Number of steps|`Array`|`false`|1|
|step|Starting step|`Number`|`false`|1|
|back|-|`Boolean`|`false`|-|

<!-- @vuese:ve-stepper:props:end -->


## Events

<!-- @vuese:ve-stepper:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|btn-back-click|Handles clicks on the 'back' button|-|
|btn-next-click|Handles clicks on the 'next' button|-|
|step-click|Handles clicks on the step buttons|-|

<!-- @vuese:ve-stepper:events:end -->



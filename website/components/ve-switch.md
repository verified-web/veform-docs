# ve-switch

Demo: https://demo-veform.web.verified.eu/#/ve-switch

## Props

<!-- @vuese:ve-switch:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|id|The unique id of the switch|`String`|`true`|-|
|rules|Used to set if the switch is required or not|`String`|`false`|-|

<!-- @vuese:ve-switch:props:end -->


## Events

<!-- @vuese:ve-switch:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|Updates the v-model|Boolean value representing the switch state|
|on-click|Triggers an on-click method if set in component|Boolean value representing the switch state|

<!-- @vuese:ve-switch:events:end -->


## Slots

<!-- @vuese:ve-switch:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|Switch label (optional)|`<label></label>`|

<!-- @vuese:ve-switch:slots:end -->



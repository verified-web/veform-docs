# ve-textarea

Demo: https://demo-veform.web.verified.eu/#/ve-textarea

## Props

<!-- @vuese:ve-textarea:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|-|`String` /  `Number`|`false`|-|
|rows|-|`String`|`false`|2|
|rules|-|`String`|`false`|-|
|placeholder|Placeholder text shown in the textarea until replaced|`String`|`false`|-|
|mask|-|—|`false`|-|
|blurvalidate|-|—|`false`|-|

<!-- @vuese:ve-textarea:props:end -->


## Events

<!-- @vuese:ve-textarea:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|

<!-- @vuese:ve-textarea:events:end -->


